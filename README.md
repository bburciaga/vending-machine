# Vending Machine

## Purpose

Develop a vending machine that applies a Real Time Operating System (RTOS) called FreeRTOS. It is to help understand queues, tasks, and semiphores/mutex's.

## Board

STM32F407 DISC-1

## Tools

STM CubeMX

> Assign the specific pins and LEDs to be programmed too. However, it is not needed as long as you know how to assign them in the code directly.

STM CubeIDE

> The class I was in used Keil to write code and program directly to the board, but it did not run on Linux so I defaulted to STM CubeIDE. It does the same thing, but I found it more intuitive.

FreeRTOS

> Operating system used for the STM32F407 DISC-1

## Installation

## How to edit

Most of the code will be done through freertos.c, but other configurations like tick rate can be accessed through FreeRTOSConfig.h
