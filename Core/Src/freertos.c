#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* USER CODE END Variables */
osThreadId coinTaskHandle;
osThreadId selectTaskHandle;
osThreadId dispenseTaskHandle;
osThreadId cashbackTaskHandle;
osMessageQId selectQueueHandle;
osMessageQId coinQueueHandle;
osMessageQId restartQueueHandle;
osMessageQId cashbackQueueHandle;

void StartCoinTask(void const * argument);
void StartSelectTask(void const * argument);
void StartDispenseTask(void const * argument);
void StartCashbackTask(void const * argument);

extern void MX_USB_HOST_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize ) {
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

xSemaphoreHandle dispenseKey = 0;

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN RTOS_SEMAPHORES */
  dispenseKey = xSemaphoreCreateBinary();

  /* Create the queue(s) */
  /* definition and creation of selectQueue */
  osMessageQDef(selectQueue, 16, uint16_t);
  selectQueueHandle = osMessageCreate(osMessageQ(selectQueue), NULL);

  /* definition and creation of coinQueue */
  osMessageQDef(coinQueue, 16, uint16_t);
  coinQueueHandle = osMessageCreate(osMessageQ(coinQueue), NULL);

  osMessageQDef(restartQueue, 16, uint16_t);
  restartQueueHandle = osMessageCreate(osMessageQ(restartQueue), NULL);

  osMessageQDef(cashbackQueue, 16, uint16_t);
  cashbackQueueHandle = osMessageCreate(osMessageQ(cashbackQueue), NULL);

  /* Create the thread(s) */
  /* definition and creation of coinTask */
  osThreadDef(coinTask, StartCoinTask, osPriorityNormal, 0, 128);
  coinTaskHandle = osThreadCreate(osThread(coinTask), NULL);

  /* definition and creation of selectTask */
  osThreadDef(selectTask, StartSelectTask, osPriorityNormal, 0, 128);
  selectTaskHandle = osThreadCreate(osThread(selectTask), NULL);

  /* definition and creation of dispenseTask */
  osThreadDef(dispenseTask, StartDispenseTask, osPriorityLow, 0, 128);
  dispenseTaskHandle = osThreadCreate(osThread(dispenseTask), NULL);

  osThreadDef(cashbackTask, StartCashbackTask, osPriorityHigh, 0, 128);
  cashbackTaskHandle = osThreadCreate(osThread(cashbackTask), NULL);
}

void StartCoinTask(void const * argument) {
  /* init code for USB_HOST */
  MX_USB_HOST_Init();
  /* USER CODE BEGIN StartCoinTask */
  int txCoin = 0;
  int rxCoin = 0;
  /* Infinite loop */
  for(;;) {
    if (xQueueReceive(restartQueueHandle, &rxCoin, 250)) {
      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 0);
      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 0);
      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 0);
      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0);
      txCoin = 0;
    }

    if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)) txCoin = txCoin + 1;

    for (int i = 0; i < txCoin; i++) {
      HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
      HAL_Delay(250);
      HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
      HAL_Delay(250);
    }
    HAL_Delay(250);

    xQueueSend(coinQueueHandle, &txCoin, 250);
    osDelay(1);
  }
  /* USER CODE END StartCoinTask */
}

void StartSelectTask(void const * argument) {
  /* USER CODE BEGIN StartSelectTask */
  int rxCoin = 0;
  int txCoin = 0;
  int txSelect = 0;
  int txCount = 0;

  for(;;) {
    if (xSemaphoreTake(dispenseKey, 250)) {
      if (xQueueReceive(coinQueueHandle, &rxCoin, 250)) {
        if (rxCoin > 0) {
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 1);
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 1);
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 1);
          if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_1)) {
            if (rxCoin > 1) {
              txCount = txCount + 1;
              txSelect = 1;
            }
          }
          if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2)) {
            if (rxCoin > 1) {
              txCount = txCount + 1;
              txSelect = 2;
            }
          }
          if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_3)) {
            if (rxCoin > 1) {
              txCount = txCount + 1;
              txSelect = 3;
            }
          }
          if (txSelect > 0) {
            if (txCount == 1) {
              HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 0);
              HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 0);
              HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 0);
              HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0);
              vTaskDelay(250);
              txCoin = rxCoin - 2;
              if (xQueueSend(selectQueueHandle, &txSelect, 250)) txSelect = 0;
              if (xQueueSend(cashbackQueueHandle, &txCoin, 250)) {
                rxCoin = 0;
                txCoin = 0;
                txCount = 0;
              }
            } else if (txCount > 1) {
              HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, 1);
              vTaskDelay(1000);
              HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, 0);
              txCoin = rxCoin;
              if (xQueueSend(cashbackQueueHandle, &txCoin, 250)) {
                txCoin = 0;
                txSelect = 0;
                txCount = 0;
              }
            }
          }
        }
      }
    }
    xSemaphoreGive(dispenseKey);

    osDelay(1);
  }
  /* USER CODE END StartSelectTask */
}

void StartDispenseTask(void const * argument) {
  /* USER CODE BEGIN StartDispenseTask */
  int rxSelect = 0;
  /* Infinite loop */
  for(;;) {
    if (xSemaphoreTake(dispenseKey, 250)) {
      if (xQueueReceive(selectQueueHandle, &rxSelect, 250)) {
        switch (rxSelect) {
        case 1:
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 1);
          HAL_Delay(1000);
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 0);
          break;
        case 2:
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 1);
          HAL_Delay(1000);
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 0);
          break;
        case 3:
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 1);
          HAL_Delay(1000);
          HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0);
          break;
        }
        rxSelect = 0;
      }
    }
    xSemaphoreGive(dispenseKey);
    osDelay(1);
  }
}

void StartCashbackTask (void const * argument) {
  int rxCoin = 0;
  int txReset = 0;

  for (;;) {
    if (xQueueReceive(cashbackQueueHandle, &rxCoin, 250)) {
      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, 0);
      for (int i = 0; i < rxCoin; i++) {
        HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, 1);
        vTaskDelay(1000);
        HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, 0);
        vTaskDelay(1000);
      }

      if (xQueueSend(restartQueueHandle, &txReset, 250)) rxCoin = 0;
    }
    osDelay(1);
  }
}
